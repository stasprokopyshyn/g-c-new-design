const webpack = require('webpack')
const path = require('path')

module.exports = {
    entry: './src/js/index.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: '/node_modules/',
                options: {
                    presets: ['@babel/preset-env', '@babel/preset-react']
                }
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                exclude: [path.resolve(__dirname,'./src/img/')],
                loader: 'file-loader',
                options: {
                    publicPath: '../fonts/', //<--resolve the path in css files
                    outputPath: 'fonts/',
                    name: '[name].[ext]'
                }
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                exclude: [path.resolve(__dirname,'./src/fonts/')],
                loader: 'file-loader',
                options: {
                    publicPath: '../img/', //<--resolve the path in css files
                    outputPath: 'img/',
                    name: '[name].[ext]'
                },
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    //"style-loader",
                    // {
                    //     loader: MiniCssExtractPlugin.loader
                    // },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true,
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        // new FixStyleOnlyEntriesPlugin(),
        // new MiniCssExtractPlugin({
        //     filename: "css/[name].css"
        // })
    ]
};